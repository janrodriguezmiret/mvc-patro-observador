package edu.damo.mvc;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class VistaDeLlistaEstudiants extends ListView implements ConjuntEstudiants.ModificacioObservadorConjuntEstudiants { // la Vista

    private ConjuntEstudiants estudiants; // el Model
    private ArrayAdapter<Estudiant> adapter; // l'adapter per visualitzar les dades del Model

    /**
     * Constructor usat per crear una llista manualment des del codi
     *
     * @param context
     */
    public VistaDeLlistaEstudiants(Context context) {
        super(context);
    }

    /**
     * Constructor usat pel framework per inflar la vista des de l'XML
     *
     * @param context
     * @param attrs
     */
    public VistaDeLlistaEstudiants(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Constructor usat pel framework per inflar la vista des de l'XML
     * aplicant un estil base d'un Tema
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    public VistaDeLlistaEstudiants(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Vincula el Model a la Vista i estableix la Vista com a observador del Model
     *
     * @param estudiants
     */
    public void vinculaModel(ConjuntEstudiants estudiants) {
        this.estudiants = estudiants;
        this.estudiants.vinculaObservadorConjuntEstudiants(this);
        adapter = new ArrayAdapter<>(getContext(), R.layout.estudiant_item, estudiants.getEstudiants());
        setAdapter(adapter);
    }

    /**
     * Implementa l'interfície d'observabilitat definida en el Model (observable)
     * Cal avisar a l'adapter que les dades han canviat
     */
    public void onModificaConjuntEstudiants() {
        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

}
