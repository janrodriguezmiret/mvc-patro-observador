package edu.damo.mvc;

import java.util.ArrayList;
import java.util.List;

public class ConjuntEstudiants { // el Model

    /**
     * Interfície d'observabilitat que cal que implementin els observadors d'aquest Model
     */
    public interface ModificacioObservadorConjuntEstudiants {
        void onModificaConjuntEstudiants();
    }

    /**
     * El Model en sí, el conjunt de dades amb les quals treballarem
     */
    private List<Estudiant> estudiants;


    /**
     * Constructor amb un conjunt d'estudiants buit
     */
    public ConjuntEstudiants() {
        estudiants = new ArrayList<>();
    }

    /**
     * Constructor amb nombre d'estudiants inicials
     */
    public ConjuntEstudiants(int numEstudiants) {
        estudiants = new ArrayList<>();
        for (int i = 0; i < numEstudiants; i++) {
            afegeixEstudiant(new Estudiant());
        }
    }

    /**
     * La llista d'observadors, als quals se'ls notificarà de canvis en el Model
     */
    private List<ModificacioObservadorConjuntEstudiants> observadors = new ArrayList<>();

    /**
     * @param observador L'observador a afegir a la llista d'observadors vinculats
     */
    public void vinculaObservadorConjuntEstudiants(ModificacioObservadorConjuntEstudiants observador) {
        observadors.add(observador);
    }

    /**
     * Mètodes públics que fa servir el Controlador per modificar el Model
     */
    public void afegeixEstudiant(Estudiant estudiant) {
        estudiants.add(0,estudiant);
        notificaObservadors();
    }

    public void esborraTotsEstudiants() {
        estudiants.clear();
        notificaObservadors();
    }

    public void esborraUnEstudiant() {
        if (!estudiants.isEmpty()) {
            estudiants.remove(0);
            notificaObservadors();
        }
    }

    /**
     * Notifica a tots els observadors que hi ha hagut un canvi en el Model
     */
    private void notificaObservadors() {
        if (observadors != null) {
            for (ModificacioObservadorConjuntEstudiants observador:observadors) {
                observador.onModificaConjuntEstudiants();
            }
        }
    }

    /**
     * @return La llista d'estudiants actual del Model
     */
    public List<Estudiant> getEstudiants() {
        return estudiants;
    }

}
