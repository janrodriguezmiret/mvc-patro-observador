package edu.damo.mvc;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.time.LocalDate;

public class GeneradorEstudiant {
    private static String[] Beginning = { "Kr", "Ca", "Ra", "Mrok", "Cru",
            "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar", "Mjol",
            "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro",
            "Mar", "Luk" };
    private static String[] Middle = { "air", "ir", "mi", "sor", "mee", "clo",
            "red", "cra", "ark", "arc", "miri", "lori", "cres", "mur", "zer",
            "marac", "zoir", "slamar", "salmar", "urak" };
    private static String[] End = { "d", "ed", "ark", "arc", "es", "er", "der",
            "tron", "med", "ure", "zur", "cred", "mur" };



    public static String getNom(){

        Random rand = new Random();
        return Beginning[rand.nextInt(Beginning.length)] +
                Middle[rand.nextInt(Middle.length)]+
                End[rand.nextInt(End.length)];

    }
    public static int genNumDni(){
        return ThreadLocalRandom.current().nextInt(40000000, 50000000);
    }

    public static String getDni(){
        int n = genNumDni();
        char c = genLletraDni(n);
        return (Integer.toString(n) + c);

    }
    public static LocalDate getNaixement(){
        Random random = new Random();
        int minDay = (int) LocalDate.of(1900, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2015, 1, 1).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);

        LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);

        return randomBirthDate;

    }

    public static char genLletraDni(int n){
        int num = n % 23;
        char c = 'T';
        switch(num){
            case 0:
                c = 'T';
                break;
            case 1:
                c = 'R';
                break;
            case 2:
                c = 'W';
                break;
            case 3:
                c = 'A';
                break;
            case 4:
                c = 'G';
                break;
            case 5:
                c = 'M';
                break;
            case 6:
                c = 'Y';
                break;
            case 7:
                c = 'F';
                break;
            case 8:
                c = 'P';
                break;
            case 9:
                c = 'D';
                break;
            case 10:
                c = 'X';
                break;
            case 11:
                c = 'B';
                break;
            case 12:
                c = 'N';
                break;
            case 13:
                c = 'J';
                break;
            case 14:
                c = 'Z';
                break;
            case 15:
                c = 'S';
                break;
            case 16:
                c = 'Q';
                break;
            case 17:
                c = 'V';
                break;
            case 18:
                c = 'H';
                break;
            case 19:
                c = 'L';
                break;
            case 20:
                c = 'C';
                break;
            case 21:
                c = 'K';
                break;
            case 22:
                c = 'E';
                break;
        }
        return c;
    }



}
