package edu.damo.mvc;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class Estudiant {
    
    private String nom;
    private String cognom;
    private LocalDate naixement;
    private String dni;


    /**
     * Constructor manual
     *
     * @param nom
     * @param cognom
     * @param naixement
     * @param dni
     */
    public Estudiant(String nom, String cognom, LocalDate naixement, String dni){
        this.nom = nom;
        this.cognom = cognom;
        this.naixement = naixement;
        this.dni = dni;
    }

    /**
     * Constructor automàtic 
     */
    public Estudiant(){
        nom = GeneradorEstudiant.getNom();
        cognom = GeneradorEstudiant.getNom();
        naixement = GeneradorEstudiant.getNaixement();
        dni = GeneradorEstudiant.getDni();

    }

    /**
     * L'ArrayAdapter cridarà a Estudiant.toString() per a passar l'objecte
     * Estudiant a String.
     * Per tant, cal indicar-li com volem que ho faci.
     */
    @Override
    public String toString() {
        DateTimeFormatter formatejador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return nom + ' ' + cognom + "\n" + naixement.format(formatejador) + "\n" + dni;
    }





}
