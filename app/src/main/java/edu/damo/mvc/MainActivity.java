package edu.damo.mvc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity { // el Controlador

    VistaDeLlistaEstudiants vistaDeLlistaEstudiants; // la Vista
    ConjuntEstudiants conjuntEstudiants; // el Model

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicialitza();
    }

    /**
     * Inicialitza el Controlador
     * Obtenim la Vista i el Model i els lliguem
     * Definim el comportament dels botons
     */
    private void inicialitza() {
        vistaDeLlistaEstudiants = findViewById(R.id.vistadellistaestudiants);
        conjuntEstudiants = new ConjuntEstudiants(4);
        vistaDeLlistaEstudiants.vinculaModel(conjuntEstudiants);

        findViewById(R.id.botoafegeix).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        afegeixEstudiant();
                    }
                }
        );

        findViewById(R.id.botoesborraun).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        esborraUnEstudiant();
                    }
                }
        );

        findViewById(R.id.botoesborratots).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        esborraTotsEstudiants();
                    }
                }
        );
    }

    /**
     * Mètodes privats per modificar el Model
     */
    private void afegeixEstudiant() {
        Estudiant nouEstudiant = new Estudiant();
        conjuntEstudiants.afegeixEstudiant(nouEstudiant);
    }

    private void esborraUnEstudiant() {
        conjuntEstudiants.esborraUnEstudiant();
    }

    private void esborraTotsEstudiants() {
        conjuntEstudiants.esborraTotsEstudiants();
    }
}
